using System;
using Xunit;
using asanca;

namespace test
{
    public class MyMathTest
    {
        [Fact]
        public void TestAdd()
        {
            MyMath my = new MyMath();
            int res = my.add(23, 43);
            Assert.Equal(res, 23 + 43);
        }
    }
}
