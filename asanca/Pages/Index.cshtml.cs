﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using asanca;

namespace asanca.Pages
{
    public class IndexModel : PageModel
    {
        private int result;

        public void OnGet()
        {
            MyMath my = new MyMath();
            result = my.add(42, 37);
        }
    }
}
